﻿using System;

namespace EF_Caso2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("...............Distancias mínimas y recorrido...............\n");

            //Creamos el grafo 1
            int[,] graph1 = {
               { 0,16,6,6,5,0,0},
               { 16,0,10,0,6,0,7},
               { 6,10,0,25,5,1,0},
               { 6,0,25,0,9,7,12},
               { 5,6,5,9,0,8,0},
               { 0,0,1,7,8,0,4},
               { 0,7,0,12,0,4,0}
            };

            //El camino más corto de Dijkstra
            Console.WriteLine("...............Distancias mínimas de casa a los demás puntos...............\n");
            
            Dijkstra.DijkstraAlgo(graph1, 0, 7);

            Console.WriteLine("");
            //Console.Read();
            Console.WriteLine("...............Distancias mínimas entre cada par de puntos................\n");

            const int INF = 99999;

            int[,] graph2 = {
               { 0,16,6,6,5,INF,INF},
               { 16,0,10,INF,6,INF,7},
               { 6,10,0,25,5,1,INF},
               { 6,INF,25,0,9,7,12},
               { 5,6,5,9,0,8,INF},
               { INF,INF,1,7,8,0,4},
               { INF,7,INF,12,INF,4,0}
            };
           
            //Algoritmo de Warshall
            FloydWarshall.FloydWarshallAlgo(graph2, 7);

            //Prim;

            int[,] graph3 = {
               { 0,16,6,6,5,0,0},
               { 16,0,10,0,6,0,7},
               { 6,10,0,25,5,1,0},
               { 6,0,25,0,9,7,12},
               { 5,6,5,9,0,8,0},
               { 0,0,1,7,8,0,4},
               { 0,7,0,12,0,4,0}
            };
            
            Console.WriteLine("\n");
            Console.WriteLine("..........Recorrido de mínima distancia que pase por todos los puntos..........\n");
            Prim.PrimAlgo(graph3, 7);
            Console.ReadLine();
        }
    }
}
