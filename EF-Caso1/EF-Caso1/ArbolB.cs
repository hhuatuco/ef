﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EF_Caso1
{
    class ArbolB
    {
        public Nodo raiz;
        public Nodo GetRaiz()
        {
            return raiz;
        }

        public void PreOrder(Nodo raiz)
        {
            if (raiz !=null)
            {
                Console.WriteLine("ID: {0} Prioridad: {1} Pedido: {2} Cliente: {3} Costo: {4}", raiz.pedido.ID, raiz.pedido.Prioridad, raiz.pedido.Pedido, raiz.pedido.Cliente, raiz.pedido.Costo);
                PreOrder(raiz.Izquierdo);
                PreOrder(raiz.Derecho);
            }
        }
        public void InOrder(Nodo raiz)
        {
            if (raiz != null)
            {
                InOrder(raiz.Izquierdo);
                Console.WriteLine("ID: {0} Prioridad: {1} Pedido: {2} Cliente: {3} Costo: {4}", raiz.pedido.ID, raiz.pedido.Prioridad, raiz.pedido.Pedido, raiz.pedido.Cliente, raiz.pedido.Costo);
                InOrder(raiz.Derecho);
            }
        }
        public void PostOrder(Nodo raiz)
        {
            if (raiz != null)
            {
                PostOrder(raiz.Izquierdo);
                PostOrder(raiz.Derecho);
                Console.WriteLine("ID: {0} Prioridad: {1} Pedido: {2} Cliente: {3} Costo: {4}", raiz.pedido.ID, raiz.pedido.Prioridad, raiz.pedido.Pedido, raiz.pedido.Cliente, raiz.pedido.Costo);
            }
        }
        public void InsertarNodo(int id, int prioridad,string pedido, string cliente, int costo )
        {
            Nodo Puntero;
            Nodo Padre;
            PedidosD Pedidos = new PedidosD(id, prioridad, pedido, cliente, costo);
            Nodo nodo = new Nodo
            {
                pedido = Pedidos
            };

            if (raiz != null)
            {
                Puntero = raiz;
                while (true)
                {
                    Padre = Puntero;
                    if (id < Puntero.pedido.ID)
                    {
                        Puntero = Puntero.Izquierdo;
                        if (Puntero == null)
                        {
                            Padre.Izquierdo = nodo;
                            break;
                        }
                    }
                    else
                    {
                        Puntero = Puntero.Derecho;
                        if (Puntero == null)
                        {
                            Padre.Derecho = nodo;
                            break;
                        }
                    }
                }
            }
            else
            {
                raiz = nodo;
            }
        }

    }
}
