﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EF_Caso1
{
    class PedidosD
    {
        public int ID { get; set; }
        public int Prioridad { get; set; }
        public string Pedido { get; set; }
        public string Cliente { get; set; }
        public int Costo { get; set; }

        public PedidosD (int Id, int prioridad, string pedido, string cliente, int costo)
        {
            ID = Id; Prioridad = prioridad; Pedido = pedido; Cliente = cliente; Costo = costo;

        }
    }
}
