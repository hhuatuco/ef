﻿using System;


namespace EF_Caso1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("...............Ingresando 5 pedidos...............\n");

            Console.WriteLine("...............Información por orden de llegada...............");

            ColasPrioridad.Nodo cola1, Temp;

            cola1 = ColasPrioridad.NuevoNodo(10,1,"Makis", "Arturo Castillo",20);
            cola1 = ColasPrioridad.push(cola1,13,2,"Ceviche", "Luis Peña", 25);
            cola1 = ColasPrioridad.push(cola1,18,3,"Tallarines", "Alex Lopez",18);
            cola1 = ColasPrioridad.push(cola1,21,4, "Wantan", "Susana Diaz",12);
            cola1 = ColasPrioridad.push(cola1,25,5,"Siu Mai","Daniel Perez",14);
            Temp = cola1;

            while (!ColasPrioridad.isEmpty(Temp))
            {
                Console.WriteLine("{0} ", ColasPrioridad.top(Temp));
                Temp = ColasPrioridad.pop(Temp);
            }
            Console.WriteLine("\n...............Datos tipo Arbol...............\n");

            ArbolB arbol = new ArbolB();
            arbol.InsertarNodo(10,1, "Makis", "Arturo Castillo", 20);
            arbol.InsertarNodo(13,2, "Ceviche", "Luis Peña", 25);
            arbol.InsertarNodo(18,3, "Tallarines", "Alex Lopez", 18);
            arbol.InsertarNodo(21,4, "Wantan", "Susana Diaz", 12);
            arbol.InsertarNodo(25,5, "Siu Mai", "Daniel Perez", 14);

            Console.WriteLine("...............PreOrder...............");
            arbol.PreOrder(arbol.GetRaiz());
            Console.WriteLine("\n");

            Console.WriteLine("...............InOrder...............");
            arbol.InOrder(arbol.GetRaiz());
            Console.WriteLine("\n");

            Console.WriteLine("...............PostOrder...............");
            arbol.PostOrder(arbol.GetRaiz());
            Console.WriteLine("\n");

            Console.WriteLine("...............Pedido con prioridad alta...............\n");
            cola1 = ColasPrioridad.push(cola1, 8,0, "Min Pao", "Alison Peralta", 10);
            arbol.InsertarNodo(8,0, "Min Pao", "Alison Peralta", 10);
            
            while (!ColasPrioridad.isEmpty(cola1))
            {
                Console.WriteLine("{0} ", ColasPrioridad.top(cola1));
                cola1 = ColasPrioridad.pop(cola1);
            }
            Console.WriteLine("\n");

            Console.WriteLine("...............PreOrder...............");
            arbol.PreOrder(arbol.GetRaiz());
            Console.WriteLine("\n");

            Console.WriteLine("...............InOrder...............");
            arbol.InOrder(arbol.GetRaiz());
            Console.WriteLine("\n");

            Console.WriteLine("...............PostOrder...............");
            arbol.PostOrder(arbol.GetRaiz());
            Console.WriteLine("\n");
            Console.ReadLine();

        }
    }
    class ColasPrioridad
    {
        public class Nodo
        {
            public PedidosD Pedido;
            public Nodo Siguiente;
        }
        public static Nodo nodo = new Nodo();
        public static Nodo NuevoNodo(int id, int prioridad, string pedido, string cliente, int costo)
        {
            Nodo temporal = new Nodo();
            PedidosD pedidos = new PedidosD(id, prioridad, pedido, cliente, costo);
            temporal.Pedido = pedidos;
            temporal.Siguiente = null;
            return temporal;
        }
        public static string top(Nodo cabeza)
        {
            return "ID: "+(cabeza).Pedido.ID + "  Prioridad: "+(cabeza).Pedido.Prioridad + "  Pedido: "+(cabeza).Pedido.Pedido +"  Cliente: "+(cabeza).Pedido.Cliente +"  Costo: "+(cabeza).Pedido.Costo;
        }
        public static Nodo pop(Nodo cabeza)
        {
            Nodo temp = cabeza;
            (cabeza) = (cabeza).Siguiente;
            return cabeza;
        }

        public static Nodo push(Nodo cabeza, int id, int prioridad, string pedido, string cliente, int costo)
        {
            Nodo puntero = (cabeza);
            Nodo temporal = NuevoNodo(id, prioridad, pedido, cliente, costo);
            if ((cabeza).Pedido.Prioridad > prioridad)
            {
                temporal.Siguiente = cabeza;
                (cabeza) = temporal;
            }
            else
            {
                while (puntero.Siguiente != null &&
                    puntero.Siguiente.Pedido.Prioridad < prioridad)
                {
                    puntero = puntero.Siguiente;
                }
                temporal.Siguiente = puntero.Siguiente;
                puntero.Siguiente = temporal;
            }
            return cabeza;
        }

        public static bool isEmpty(Nodo cabeza)
        {
            if (cabeza == null)
            {
                return true;
            }
            return false;
        }

    }
}
